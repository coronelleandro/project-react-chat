import React from 'react';
import ReactDom from 'react-dom';
import App from './App';

const body =  <App/>

const container = document.getElementById('root');

ReactDom.render(body,container)