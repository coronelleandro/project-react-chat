import React, { useEffect } from 'react'
import './css/App.css';
import UserFormComponent from './components/registrar/userFormComponent';
import SingInComponent from './components/ingresar/singInComponent';
import UsuariosChatComponent from './components/usuariosChat/usuariosChatComponent';
import {BrowserRouter as Router,Switch,Route,Link,useRouteMatch,useParams} from "react-router-dom";

const App = () =>{
    
    useEffect(()=>{
        fetch('http://localhost:3000/api/users')
        .then(res => console.log(res))
        .catch(error => {
            console.log(error);
        })
    },[]);

    return(
        <>
        <Router>
            <div>
                <header className="header">
                    <div className="divHeader">
                        <div className="logo"></div>
                        <div className="menu">
                            <Link to="/registrar" className="link">Registrar</Link>
                            <Link to="/ingresar" className="link">Ingresar</Link>
                        </div>
                    </div>
                </header>
                <Switch>
                 
                    <Route path="/registrar">
                        <UserFormComponent/>
                    </Route>
                    <Route path="/ingresar">
                        <SingInComponent/>
                    </Route>
                    <Route path="/chat">
                        <UsuariosChatComponent/>
                    </Route>
                </Switch>
                
            </div>
        </Router>
        </>
    )
}

export default App;