import React, { useEffect, useState } from 'react';
import {BrowserRouter as Router, useHistory } from "react-router-dom";
import './css/ingresar.css';

const SingInComponent = (props) => {

    const [email,setEmail] = useState({value:null,errorValue:null});
    const [password,setPassword] = useState({value:null,errorValue:null});
    let history = useHistory()

    useEffect(()=>{
        setEmail({value:null,errorValue:null});
        setPassword({value:null,errorValue:null});
    },[])

    useEffect(()=>{
    },[email,password])

    const validar = (event) => {
        event.preventDefault();
        let valid = true;
        if(event.target.email.value == ''){
            setEmail({errorValue:<div className="errorLogin">El email es requerido</div>});
            valid = false; 
        }
        else{
            setEmail({errorValue:null});
        }

        if(event.target.password.value == ''){
            setPassword({errorValue:<div className="errorLogin">El password es requerido</div>});
            valid = false;
        }
        else{
            setPassword({errorValue:null});
        }

        if(valid == true){
            
            goChat();
        }
    }

    const goChat = () =>{
        history.push('/chat');
    }

    return(
        <section className="sectionSignIn">
            <h1>Crear</h1>
            <form className="formSingIn" onSubmit={(res)=>validar(res)}>
                <div className="divInput">                
                    <input type="email" className="inputCreate" name="email" placeholder="email"/>
                    {email.errorValue}
                </div>
                <div className="divInput">                
                    <input type="password" className="inputCreate" name="password" placeholder="password"/>
                    {password.errorValue}
                </div>
                <div className="divSingIn">
                    <button className="signIn">Ingresar</button>
                </div>
            </form>
        </section>
    )
}

export default SingInComponent;