import React, { useEffect, useState } from 'react';
import './css/userForm.css';
import {BrowserRouter as Router, useHistory } from "react-router-dom";


const UserFormComponent = (props) => {

    const [nombre,setNombre] = useState({value:null,errorValue:null});
    const [nombreUsuario,setNombreUsuario] = useState({value:null,errorValue:null});
    const [email,setEmail] = useState({value:null,errorValue:null});
    const [password,setPassword] = useState({value:null,errorValue:null});
    let history = useHistory();

    useEffect(async () =>{
        setNombre("");
        setNombreUsuario("");
        setEmail("");
        setPassword("");
        
        return () => {
            
        }
    },[]);

    useEffect(async () =>{
        return () => {
            console.log("Hola 2");
        }
    },[nombre,nombreUsuario,email,password]);

    const validar = (event) =>{
        event.preventDefault();
        let valid = true;
        if(event.target.nombre.value == ""){
            setNombre({errorValue : <div className="erroInput">El nombre es requerido</div>});
            valid = false;
        }
        else{
            setNombre({errorValue : null});
        }
        if(event.target.nombreUsuario.value == ""){
            setNombreUsuario({errorValue: <div className="erroInput">El nombre de usuario es requerido</div>})
            valid = false;
        }
        else{
            setNombreUsuario({errorValue : null});
        }
        if(event.target.email.value == ""){
            setEmail({errorValue:<div className="erroInput">El email es requerido</div>})
            valid = false;
        }
        else{
            setEmail({errorValue:null})
        }
        if(event.target.password.value == ""){
            setPassword({errorValue: <div className="erroInput">El password es requerido</div>})
            valid = false;
        }
        else{
            setPassword({errorValue:null})
        }
        
        if(valid == true){
            createUsuario(event.target);
        }
    } 

    const createUsuario = (values) =>{
       
       history.push('/ingresar')
    }

    return(
        <>
        <section className="sectionUserForm">
            <h1>Crear {props.title}</h1>
            <form className="formCreate" onSubmit={(res)=>validar(res)}>
                <div className="divInput">                
                    <input type="text" className="inputCreate" name="nombre" placeholder="Nombre"/>
                    {nombre.errorValue}
                </div>
                <div className="divInput">
                    <input type="text" className="inputCreate" name="nombreUsuario" placeholder="Nombre de Usuario"/>
                    {nombreUsuario.errorValue}
                </div>
                <div className="divInput">
                    <input type="email"  className="inputCreate" name="email" placeholder="Email"/>
                    {email.errorValue}
                </div>
                <div className="divInput">
                    <input type="password"  className="inputCreate" name="password" placeholder="Password"/>
                    {password.errorValue}
                </div>
                {
                /*<div className="divInput">
                    <input type="file" name="file" className="fileCreate" />
                </div>*/
                }
                <div className="divCreate">
                    <button className="createButton">Crear</button>
                </div>
            </form>
        </section>
        </>
    )
} 

export default UserFormComponent;