const express = require('express');
const app = express();
const {mongoose} = require('./database/database');
const cors = require('cors');
const morgan = require('morgan');

app.set('port',process.env.PORT || 3000);

app.use(morgan('dev'));
app.use(express.json());

app.use(cors({origin:'http://localhost:3001'}));

app.use('/api/users',require('./routes/user.routes'));
app.use('/api/chats',require('./routes/chat.router'));
app.use('/api/messages',require('./routes/message.router'));

app.listen(app.get('port'),()=>{
    console.log("funciono: 3000");
})

