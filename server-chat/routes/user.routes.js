const express = require('express');
const router = express.Router();
const userController = require('../controller/user.controller');

router.get('/',userController.getUsers);

router.get('/:id',userController.getOneUser);

router.post('/create',userController.createUser);

router.post('/login',userController.postUserEmail);

router.put('/update',userController.updateUser);

router.delete('/delete',userController.deleteUser);

module.exports = router;