const express = require('express');
const messageController = require('../controller/message.controller');
const router = express.Router();

router.get('/',messageController.getAllMessagesUser);

router.post('/create',messageController.createMessage);

module.exports = router;
