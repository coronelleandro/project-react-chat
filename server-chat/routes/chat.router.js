const express = require('express');
const router = express.Router();
const chatController = require('../controller/chat.controller');   

router.get('/',chatController.getChats);

router.post('/create',chatController.createChat);

module.exports = router;