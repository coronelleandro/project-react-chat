const mongoose = require('mongoose');
const {Schema} = mongoose; 

const messageSchema = Schema({
    content:{type:String,require:true},
    date:{type:Date,default:new Date},
    user:{type:Schema.ObjectId, ref: 'User'},
    chat:{type:Schema.ObjectId, ref: 'Chat'}, 
});

module.exports = mongoose.model('Message',messageSchema);