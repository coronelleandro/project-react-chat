const mongoose = require('mongoose');
const {Schema} = mongoose;

const userSchema = new Schema({
    name:{type:String,require:true},
    nameUser:{type:String,require:true},
    email:{type:String,require:true},
    password:{type:String,require:true},
    userMessages:[{
        type:Schema.ObjectId, ref: 'Message'
    }],
    userChats:[{
        type:Schema.ObjectId,ref: 'Chat' 
    }]
});

module.exports = mongoose.model('User',userSchema);