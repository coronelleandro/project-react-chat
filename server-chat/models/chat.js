const mongoose = require('mongoose');
const {Schema} = mongoose;

const chatSchema = Schema({
    userUno:{type:Schema.ObjectId, ref: 'User'},
    userDos:{type:Schema.ObjectId, ref: 'User'},
    chatMessages:[{
        type:Schema.ObjectId, ref: 'Message'
    }],
}); 

module.exports = mongoose.model('Chat',chatSchema);