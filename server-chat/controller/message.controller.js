const messageController = {};
const mongoose = require('mongoose');
const Message = require('../models/message');
const Chat = require('../models/chat');
const User = require('../models/user');

messageController.getAllMessagesUser = async(req,res)=>{
    const messages = await Message.find();
    res.json(messages);
}

messageController.createMessage = async(req,res)=>{
    const {body} = req;
    const message = new Message({
        content: body.content, 
        user: body.idUser
    });
    await message.save(message);

    const user = await User.findById(body.idUser);
    user.userMessages.push(message);
    await user.save(user);

    const chat = Chat.findById(body.idChat);
    chat.chatMessages.push(message);  
    await chat.save(chat); 

    
    res.json({'status':'guardar'});
}

module.exports = messageController;

