const userController = {};
const User = require('../models/user');
const mongoose = require('mongoose');

userController.getUsers = async(req,res)=>{
    const users = await User.find();
    res.json(users);
}

userController.getOneUser = async(req,res)=>{
    const user = await User.findById(req.params.id);
    res.json(user);
}

userController.createUser = async(req,res)=>{
    const {body} = req;
    const user = new User({
        name : body.name,
        nameUser : body.nameUser,
        email : body.email,
        password : body.password,
    })
    await user.save(user);
    res.json({'status':'exito'});
}

userController.postUserEmail = async(req,res) =>{
    const {body} = req;
    const user = await User.findOne({
        $and:[
            { email: body.email },
            { password: body.password }
        ]
    })
    res.json(user);
}

userController.updateUser = async(req,res) =>{    
    const {body} = req;
    const user = {
        name : body.name,
        nameUser : body.nameUser,
        email : body.email,
        password : body.password
    } 
    await User.findByIdAndUpdate( body.id, {$set:user}, {$new:true} );
    res.json({'status':'exito'});
}

userController.deleteUser = async(req,res)=>{
    const {body} =  req;
    await User.findByIdAndDelete(body.id)
    res.json({'status':'exito'});
}

module.exports = userController;