const chatController = {};
const mongoose = require('mongoose');
const Chat = require('../models/chat');
const User = require('../models/user');

chatController.getChats = async(req,res)=>{
    const chats = await Chat.find();
    res.json(chats);
}

chatController.createChat = async(req,res)=>{
    const {body} = req; 
    const chat = new Chat({
        userUno: body.userUno,
        userDos: body.userDos
    });
    chat.save(chat);

    const userUno = User.findById(body.userUno);
    userUno.userChats.push(chat);
    await userUno.save(userUno);
    
    const userDos = User.findById(body.userDos);
    userDos.userChats.push(chat);
    await userDos.save(userDos);

    res.json(chat);
}

module.exports = chatController;