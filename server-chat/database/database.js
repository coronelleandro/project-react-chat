const mongoose = require('mongoose');

const URI = 'mongodb://127.0.0.1:27017/user-chat';

mongoose.connect(URI)
    .then(()=>{console.log('conectado')})
    .catch((err)=>{console.log(err)})

module.exports = mongoose;
